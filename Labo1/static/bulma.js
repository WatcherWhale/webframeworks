let button1, button2, button3;
let card1, card2, card3;
let manu, color, car;

window.addEventListener("popstate", e => {
    if(e.state)
    {
        showPage(e.state);
    }
    else
    {
        showPage(window.location.href.split("#")[1]);
    }
});

document.addEventListener("DOMContentLoaded", () => {
    button1 = document.getElementById("page1");
    button2 = document.getElementById("page2");
    button3 = document.getElementById("page3");

    card1 = document.getElementById("card1");
    card2 = document.getElementById("card2");

    button1.addEventListener("click", updatePage)
    button2.addEventListener("click", updatePage)
    button3.addEventListener("click", updatePage)


    manu = document.getElementById("manu");
    color = document.getElementById("color");
    car = document.getElementById("car");

    manu.addEventListener("change", updateSelect);
    color.addEventListener("change", updateSelect);

    downloadTable();

});

function downloadTable()
{
    const html = doRequest("http://" + window.location.host + "/table.html", "GET");
    card1.parentElement.innerHTML += html;

    card3 = document.getElementById("card3");

    card3.classList.toggle("is-hidden", true)
    card3.classList.toggle("is-6", true)
    card3.classList.toggle("collumn", true)
}

function doRequest(path, method)
{
    const xhr = new XMLHttpRequest();
    xhr.open(method, path, false);

    xhr.send();

    if(xhr.status != 200) return null;

    return xhr.response;
}

function updateSelect()
{
    if(manu.value != "" && color.value == "")
    {
        const option = Math.round(color.options.length * Math.random());
        color.selectedIndex = option + 1;
        updateSelect();
    }
    else if(manu.value != "")
    {
        car.innerText = "You selected a " + color.value + " " + manu.value;
        setTimeout(() => {
            car.innerText = "";
        }, 2000);
    }
}

function updatePage(e)
{
    history.pushState(e.target.id, "", "#" + e.target.id);
    showPage(e.target.id);
}

function showPage(page)
{
    if(page == "page1")
    {
        button1.classList.toggle("is-info", true);
        button1.classList.toggle("disabled", true);

        button2.classList.toggle("is-info", false);
        button2.classList.toggle("disabled", false);
        button3.classList.toggle("is-info", false);
        button3.classList.toggle("disabled", false);

        card1.classList.toggle("is-hidden", false);
        card2.classList.toggle("is-hidden", true);
        card3.classList.toggle("is-hidden", true);
    }
    else if(page == "page2")
    {
        button2.classList.toggle("is-info", true);
        button2.classList.toggle("disabled", true);

        button1.classList.toggle("is-info", false);
        button1.classList.toggle("disabled", false);
        button3.classList.toggle("is-info", false);
        button3.classList.toggle("disabled", false);

        card1.classList.toggle("is-hidden", true);
        card2.classList.toggle("is-hidden", false);
        card3.classList.toggle("is-hidden", true);
    }
    else if(page == "page3")
    {
        button3.classList.toggle("is-info", true);
        button3.classList.toggle("disabled", true);

        button1.classList.toggle("is-info", false);
        button1.classList.toggle("disabled", false);
        button2.classList.toggle("is-info", false);
        button2.classList.toggle("disabled", false);

        card1.classList.toggle("is-hidden", true);
        card2.classList.toggle("is-hidden", true);
        card3.classList.toggle("is-hidden", false);
    }
    else
    {
        window.pushState("page1", "", "page1");
        showPage("page1");
    }
}
