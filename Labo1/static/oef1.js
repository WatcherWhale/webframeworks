
document.addEventListener("DOMContentLoaded", () => {
    const input = document.getElementById("repeat");
    const label = document.getElementById("label");

    // On mouseleave
    input.addEventListener("change", e => {
        label.innerText = e.target.value;
    });

    // Dynamic
    input.addEventListener("input", e => {
        label.innerText = e.target.value;
    });
});
