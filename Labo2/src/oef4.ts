console.log("---===Filter===---")

const names : string[] = [ 'Silke', 'Nabil', 'Muhammed', 'Sven', 'Andie', 'Kelly' ];

let four = names.filter((val, index, arr) => val.length === 4);
console.log(four);

let sss = names.filter((val, index, arr) => val.toLowerCase()[0] === "s");
console.log(sss);

let even = names.filter((val, index, arr) => index % 2 === 0);
console.log(even);

console.log("---===Sort===---");

let up = names.sort((a, b) => a.length - b.length);
console.log(up);
let down = names.sort((a, b) => b.length - a.length);
console.log(down);


