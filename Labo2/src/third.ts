import * as math from 'mathjs';
import _ from 'lodash';
import moment from 'moment';

const unit = math.unit(50, "inch");
console.log(unit.toNumber("cm"));

console.log(_.padStart("17", 3, "0"));

console.log(moment().subtract(10, 'days').calendar());
console.log(moment().subtract(6, 'days').calendar());
console.log(moment().subtract(3, 'days').calendar());
console.log(moment().subtract(1, 'days').calendar());
console.log(moment().calendar());
console.log(moment().add(1, 'days').calendar());
console.log(moment().add(3, 'days').calendar().toLocaleLowerCase("nl-be"));
console.log(moment().add(10, 'days').calendar().toLocaleLowerCase("nl-be"));
