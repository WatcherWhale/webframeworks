export class GMClass
{
    get CheckSum()
    {
        const id = (this.year % 1000) * 1000 * 10000 + this.clientId * 1000 + this.orderId;
        return id % 97;
    }

    constructor(private readonly year: number, private readonly clientId: number = 0, private readonly orderId: number = 0) { }

    toString()
    {
        const yr = this.year % 1000;

        return "+++" + GMClass.leftPad(yr, 3) + "/" + GMClass.leftPad(this.clientId, 4) + "/" +
            GMClass.leftPad(this.orderId,3) + GMClass.leftPad(this.CheckSum, 2) + "+++";

    }

    private static leftPad(num: number, count: number) : string
    {
        let str = num.toString();

        while(str.length < count)
        {
            str = "0" + str;
        }

        return str;
    }

    public static isValid(gm: string) : boolean
    {
        const num = parseInt(gm.replace(/\+|\//g, ""))
        const check = num % 100;

        const id = (num - check) / 100;

        return id % 97 === check;
    }
}

//console.log(new GMClass(2017, 123, 1).toString())
//console.log(new GMClass(17, 123, 1).toString())
//console.log(new GMClass(2017).toString());
//
//console.log(GMClass.isValid("+++017/0000/00031+++"));
//console.log(GMClass.isValid("+++017/0000/05031+++"));
