import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent implements OnInit {

  temp = [
    {
      "city": "Anchorage",
      "temp": 0
    },
    {
      "city": "Brussel",
      "temp": 0
    },
    {
      "city": "Osaka",
      "temp": 0
    },
    {
      "city": "Auckland",
      "temp": 0
    },
    {
      "city": "Kaapstad",
      "temp": 0
    }
  ]

  degrees = [0, 16, 2, 10, 16];


  people = [new Person("Mathias", "Maes", new Date(2000, 6, 12)),new Person("Sedrik", "Balk", new Date(1880, 7, 5)),
  new Person("Pieter", "Franc", new Date(2000, 10, 5)), new Person("Dom", "DeGroene", new Date(1999, 5, 30))]

  numbers = [2,4,5,6,8,9,10,14];

  constructor() {
    this.generate();
    setInterval(() => this.generate(), 2000);
  }

  ngOnInit(): void {
  }

  generate()
  {
    let arr= [];
    for(let i = 0; i < 5; i++)
    {
      this.temp[i].temp = Math.round(Math.random() * 25);
    }
  }

  removeUneven(arr: number[]) : number[]
  {
    const a = [];

    for (let num in arr)
    {
      if(arr[num] % 2 == 0) a.push(arr[num])
    }

    return a;
  }

}

class Person
{
  constructor(readonly firstname : string, readonly name : string, readonly birthdate: Date) { }

  public get age() : number
  {
    let mom = moment();
    mom.subtract(this.birthdate.getFullYear(), "years");
    mom.subtract(this.birthdate.getMonth(), "months");
    mom.subtract(this.birthdate.getDate(), "days");
    return mom.year();

  }
  
}

