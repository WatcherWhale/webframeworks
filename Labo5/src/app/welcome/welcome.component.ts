import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  image: number = 1;
  public get imageUrl() : string
  {
    return "https://mdbootstrap.com/img/Photos/Slides/img%20(" + this.image + ").jpg";
  }

  constructor() {
    this.nextImage();
    setInterval(() => this.nextImage(), 5000);
  }

  ngOnInit(): void {
  }

  nextImage(): void {
    this.image = Math.round(151 * Math.random()) + 1;
  }
}
