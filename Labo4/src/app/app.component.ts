import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Labo4';

  time = new Date();
  counter = 0;
  randomNum = 0;
  score = -1;

  constructor()
  {
    setInterval(() => this.time = new Date(), 1);
    setInterval(() => this.counter++, 1);
    this.randomNum = this.random();
    setInterval(() => this.randomNum = this.random(), 1);
    setInterval(() => this.score = this.random(10), 1);
  }

  random(max = 100)
  {
    return Math.round((max - 1) * Math.random()) + 1;
  }
}
