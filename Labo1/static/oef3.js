document.addEventListener("DOMContentLoaded", () => {
    const div = document.getElementsByClassName("red")[0];
    const time = document.getElementById("time");

    div.addEventListener("mousemove", e => {
        const x = e.offsetX;
        const y = e.offsetY;

        div.innerText = "(" + x + ", " + y + ")";
    });

    setInterval(() => {
        const date = new Date(Date.now());
        time.innerText = "De huidige tijd is: " + date.toTimeString();
    }, 1000);
});
