const a : number = 5;
const b : number = 6;
const sum = a + b;

const arr : number[] = [1, 2, 3]

enum Color {
    RED,
    BLUE,
    GREEN
}

const colorArr : Color[] = [Color.RED, Color.GREEN];

const mulArr : (boolean|number)[]= [true, 45];

const tuple : [number, string, boolean] = [1, "", false];

let nullable : number|null;




