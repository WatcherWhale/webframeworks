import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-redbox',
  templateUrl: './redbox.component.html',
  styleUrls: ['./redbox.component.css']
})
export class RedboxComponent implements OnInit {

  coords = {x: 0, y: 0};

  constructor() { }

  ngOnInit(): void {
  }

  mouseEvent(e: MouseEvent) {
    this.coords.x = e.x;
    this.coords.y = e.y;
  }

}
