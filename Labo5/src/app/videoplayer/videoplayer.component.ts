import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videoplayer',
  templateUrl: './videoplayer.component.html',
  styleUrls: ['./videoplayer.component.css']
})
export class VideoplayerComponent implements OnInit {

  res = {width: 640, height: 400};

  constructor() { }

  ngOnInit(): void {
  }

  change(res: string) {
    this.res.width = parseInt(res.split('x')[0]);
    this.res.height = parseInt(res.split('x')[1]);
  }

}
