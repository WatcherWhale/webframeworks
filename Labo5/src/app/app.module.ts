import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { RedboxComponent } from './redbox/redbox.component';
import { TodoComponent } from './todo/todo.component';
import { VideoplayerComponent } from './videoplayer/videoplayer.component';
import { BettercalcComponent } from './bettercalc/bettercalc.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    CalculatorComponent,
    RedboxComponent,
    TodoComponent,
    VideoplayerComponent,
    BettercalcComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
