import { Component, OnInit } from '@angular/core';
import * as mathjs from "mathjs";

@Component({
  selector: 'app-bettercalc',
  templateUrl: './bettercalc.component.html',
  styleUrls: ['./bettercalc.component.css']
})
export class BettercalcComponent implements OnInit {

  buttons = [["1", "2", "3", "+"],
             ["4", "5", "6", "-"],
             ["7", "8", "9", "*"],
             [".", "0", "=", "/"]];

  statement = "";

  constructor() { }

  ngOnInit(): void {
  }

  add(val: string)
  {
    if(val == "=")
    {
      this.statement = mathjs.evaluate(this.statement);
      return;
    }

    this.statement += val;
  }

}
