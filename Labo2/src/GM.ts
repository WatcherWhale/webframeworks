const GMFunc = (year: number, clientId: number = 0, orderId: number = 0): string => {
    const yr = year % 1000;
    const id = yr * 1000 * 10000 + clientId * 1000 + orderId;
    const check = id % 97;

    return "+++" + leftPad(yr, 3) + "/" + leftPad(clientId, 4) + "/" + leftPad(orderId,3) + leftPad(check, 2) + "+++";
}

const leftPad = (num: number, count: number) : string => {
    let str = num.toString();

    while(str.length < count)
    {
        str = "0" + str;
    }

    return str;
}

console.log(GMFunc(2017, 123, 1))
console.log(GMFunc(17, 123, 1))
console.log(GMFunc(2017))
