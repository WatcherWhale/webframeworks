const square = (a : number) => a ** 2;

const somethig = (text : string, count: number) : string[] => {

    let arr : string[] = [];

    for(let i = 0; i < count; i++)
    {
        arr.push(text);
    }

    return arr;
}

(str: string, sep: string = "'") : string[] => str.split(sep);

const product = (...numbers: number[]) : number => {
    let product = numbers[0];

    for(let i = 1; i  < numbers.length; i++)
    {
        product *= numbers[i];
    }

    return product;
}

product(1, 2, 3, 4);
