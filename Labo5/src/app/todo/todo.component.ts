import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  item: string = "";
  items: string[] = [];

  constructor() { }

  ngOnInit(): void {
    console.log("hey")
  }

  add(item: string) {
    this.items.push(item);
  }

  delete(item: string) {
    const i = this.items.indexOf(item);
    this.items.splice(i, 1);
  }

}
